package com.fantasyWar.squads;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.fantasyWar.enums.SquadState;
import com.fantasyWar.enums.UnitState;
import com.fantasyWar.unit.Unit;
import com.fantasyWar.unit.UnitInfo;
import com.fantasyWar.world.World;

/**
 * Created by ObbinkM on 28-4-2014.
 */
public class Squad {
    // Radius voor selectie
    public final float radius;
    // Lijst met units
    public Array<Unit> units;
    // Gemiddelde positie
    public Vector3 position = new Vector3();
    public SquadState state = SquadState.IDLE;
    // Font voor label
    protected BitmapFont font;
    // The world
    protected World world;
    protected Squad targetSquad;
    // Squad name
    private String name;
    // Team
    private int team;
    // Label boven unit
    private Label label;
    // Layer waarop label getekend word
    private Stage stage;
    private int id;
    private float range;
    private int discipline;
    private Vector3 target = new Vector3();

    public Squad(int id, Array<Unit> units, String name, Stage stage, int team, World world) {
        this.id = id;
        this.units = units;
        this.name = name;
        this.stage = stage;
        this.team = team;
        this.world = world;
        this.range = 10.0f;
        this.discipline = 5;
        this.radius = 0.75f;
        this.font = new BitmapFont();

        Label.LabelStyle style = new Label.LabelStyle(font, Color.WHITE);
        this.label = new Label("", style);
        this.stage.addActor(label);
    }


    public void act(float delta) {

        for (final Unit instance : units) {
            instance.act(delta);
        }
        label.setText(name + " (" + units.size + ")");
    }

    public void render(float delta, Camera cam, ModelBatch modelBatch, Environment environment) {
        Vector3 avg = new Vector3();
        int count = 0;
        boolean moving = false;
        for (final Unit instance : units) {
            if (instance.state == UnitState.MOVE) {
                moving = true;
            }

            if (isVisible(cam, instance)) {
                Vector3 pos = new Vector3();
                instance.transform.getTranslation(pos);
                count++;
                avg.add(pos);
                //     modelBatch.render(instance.physicsModel.getCollisionShape().);
                modelBatch.render(instance, environment);
                //visibleCount++;
            }
        }
        if (count > 0) {
            if (moving && state != SquadState.MOVING) {

                state = SquadState.MOVING;
            } else if (!moving && state == SquadState.MOVING) {
                System.out.println("idling");
                state = SquadState.IDLE;
            }

            label.setVisible(true);
            avg.scl(1.0f / (float) count);

            position.set(avg);
            avg.y = position.y + 2;
            Vector3 proj = cam.project(avg);
            label.setPosition(proj.x, proj.y);
        } else {
            label.setVisible(false);
        }
    }

    protected boolean isVisible(final Camera cam, final Unit instance) {
        Vector3 unitPos = new Vector3();
        instance.transform.getTranslation(unitPos);
        unitPos.add(instance.center);
        return cam.frustum.sphereInFrustum(unitPos, instance.radius);
    }

    public Unit attack(int unitId, UnitInfo arrow) {
        Unit unit = null;
        //System.out.println("UID" + unitId);
        for (Unit model : units) {
            if (model.getId() == unitId) {
                unit = model;
                break;
            }
        }

        if (unit != null) {
            unit.damage(arrow.damage);
            //System.out.println("Health" + unit.getHealth());
            if (unit.getHealth() == 0) {
                return unit;
            }
        }
        return null;
    }

    public void remove(Unit unit) {
        unit.dispose();
        units.removeValue(unit, false);
    }

    public int getId() {
        return id;
    }

    public void dispose() {
        //label.setVisible(false);
        stage.getRoot().removeActor(label);

    }

    public float getRange() {
        return range;
    }

    public int getTeam() {
        return team;
    }

    public int getDiscipline() {
        return discipline;
    }

    public Vector3 getTarget() {
        return target;
    }

    public void setTarget(Vector3 in) {
        target = new Vector3(in);
        System.out.println("Target" + in);
        System.out.println("Position " + this.position);
        for (Unit model : units) {
            Vector3 posUnit = new Vector3();
            model.transform.getTranslation(posUnit);
            Vector3 afwijking = new Vector3();

            afwijking.add(posUnit);
            afwijking.sub(position);
            Vector3 newTarget = new Vector3();
            newTarget.add(in);
            newTarget.add(afwijking);

            newTarget.y = 0.5f;
            //System.out.println(newTarget);
            model.setTarget(newTarget);
        }
    }
}
