package com.fantasyWar.squads;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.fantasyWar.enums.SquadState;
import com.fantasyWar.unit.Unit;
import com.fantasyWar.world.World;

/**
 * Created by Michel on 7-5-2014.
 */
public class MeleeSquad extends Squad {
    private float reactionTime = 2;
    private float reloadTime = 4;
    private float elapsedTime = 0;

    public MeleeSquad(int id, Array<Unit> units, String name, Stage stage, int team, World world) {
        super(id, units, name, stage, team, world);
    }


    @Override
    public void act(float delta) {

        elapsedTime = elapsedTime + delta;

        switch (state) {
            case IDLE:
                idling();
                break;
            case FINDINGTARGET:
                targeting();
            case FIREING:
                fire();
            case RELOADING:
                idling();

        }

        super.act(delta);

    }

    private void fire() {
        elapsedTime = 0;
        if (targetSquad != null && targetSquad.position.dst(position) <= getRange()) {
            //   System.out.println("FIRE");
            //world.fire(this, targetSquad);
        }
        state = SquadState.IDLE;
    }

    private void targeting() {
        elapsedTime = 0;

        for (Squad squad : world.squads) {
            if (squad.getTeam() != this.getTeam()) {
                //  System.out.println("Dist: " + squad.position.dst(position));
                if (squad.position.dst(position) <= getRange()) {
                    targetSquad = squad;
                    state = SquadState.FIREING;
                    return;
                }
            }
        }
        targetSquad = null;
        state = SquadState.IDLE;
    }

    private void idling() {
        // System.out.println(elapsedTime);
        if (elapsedTime < reactionTime) {
            return;
        }
        elapsedTime = 0;

        state = SquadState.FINDINGTARGET;
    }

    private void reloading() {
        // System.out.println(elapsedTime);
        if (elapsedTime < reactionTime) {
            return;
        }
        elapsedTime = 0;

        state = SquadState.FIREING;
    }
}
