package com.fantasyWar.utils;

/**
 * Created by ObbinkM on 30-4-2014.
 */

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;


/*
Frequency 8.0
perturb 77.0
perturbtimes 32
erode 150.0

 */
public class MapCreator {
    //just convinence methods for debug
    public static Random random = new Random();
    public static boolean[][] forest = null;
    public static int size;
    private static float waterLevel = 0.5f;
    private static float beachLevel = 0.6f;
    private static float mountainLevel = 0.9f;
    private static float forestLevel = -0.05f;
    private static HeightMap grass;

    public static BufferedImage greyWriteImage(float[][] data) {
        //this takes and array of doubles between 0 and 1 and generates a grey scale image from them
        size = data.length;
        boolean[][] water = new boolean[data.length][data[0].length];
        boolean[][] beach = new boolean[data.length][data[0].length];
        boolean[][] mountain = new boolean[data.length][data[0].length];
        float[][] mountainDiff = new float[data.length][data[0].length];
        float[][] beachDiff = new float[data.length][data[0].length];
        float[][] waterDiff = new float[data.length][data[0].length];
        //boolean [][] water = new boolean[data.length][data[0].length];
        float[][] grass = newGrass(data.length);
        forest = newForest(data.length);
        BufferedImage image = new BufferedImage(data.length, data[0].length, BufferedImage.TYPE_INT_RGB);
        float[] waterColour = new float[]{0.01f, 0.3f, 1};
        float[] groundColour = new float[]{0.2f, 0.65f, 0.2f};
        float[] forestColour = new float[]{0.1f, 0.35f, 0.1f};
        float[] beachColour = new float[]{1f, 0.95f, 0.6f};
        float[] mountainColour = new float[]{0.4f, 0.4f, 0.4f};
        float max = Float.MIN_VALUE;
        float min = Float.MAX_VALUE;
        for (int y = 0; y < data[0].length; y++) {
            for (int x = 0; x < data.length; x++) {

                if (data[x][y] > max) {
                    max = data[x][y];
                }
                if (data[x][y] < min) {
                    min = data[x][y];
                }
            }
        }

        for (int y = 0; y < data[0].length; y++) {
            for (int x = 0; x < data.length; x++) {

                data[x][y] = (data[x][y] - min) / (max - min);


                if (data[x][y] < waterLevel) {
                    waterDiff[x][y] = Math.min((waterLevel - data[x][y]) * 40f, 1.0f);
                    data[x][y] = (data[x][y] - 0.2f) + (grass[x][y] / 6);
                    water[x][y] = true;
                    beach[x][y] = false;
                    mountain[x][y] = false;
                    forest[x][y] = false;
                    //System.out.println(waterDiff[x][y]);

                } else if (data[x][y] < beachLevel) {
                    beachDiff[x][y] = Math.min((beachLevel - data[x][y]) * 50f, 1.0f);

                    data[x][y] = (data[x][y] - 0.2f) + (grass[x][y] / 10);
                    beach[x][y] = true;
                    water[x][y] = false;
                    mountain[x][y] = false;
                    forest[x][y] = false;
                } else if (data[x][y] > mountainLevel) {
                    //System.out.println(data[x][y]  + "   " + mountainLevel);
                    mountainDiff[x][y] = Math.min(Math.abs(((data[x][y] - mountainLevel) * 150)), 1.0f);

                    data[x][y] = (data[x][y] - 0.2f) + (grass[x][y] / 7);
                    water[x][y] = false;
                    forest[x][y] = false;
                    beach[x][y] = false;
                    mountain[x][y] = true;


                } else {
                    data[x][y] = (data[x][y] - 0.2f) + (grass[x][y] / 7);
                    water[x][y] = false;
                    beach[x][y] = false;
                    mountain[x][y] = false;
                }

                if (data[x][y] > 1.0) {
                    //System.out.println(data[x][y]);
                    data[x][y] = 1;
                }
                if (data[x][y] < 0) {
                    //System.out.println(data[x][y]);
                    data[x][y] = 0;
                }


                if (grass[x][y] > 1.0) {
                    //System.out.println(grass[x][y]);
                    data[x][y] = 1;
                }
                if (grass[x][y] < 0) {
                    //System.out.println(grass[x][y]);
                    grass[x][y] = 0;
                }

                Color col = null;
                if (water[x][y]) {
                    col = new Color(
                            (float) (0.05f * waterDiff[x][y]) + data[x][y] * ((beachColour[0] * (1 - waterDiff[x][y])) + (waterColour[0] * (waterDiff[x][y]))),
                            (float) (0.2f * waterDiff[x][y]) + data[x][y] * ((beachColour[1] * (1 - waterDiff[x][y])) + (waterColour[1] * (waterDiff[x][y]))),
                            (float) (0.3f * waterDiff[x][y]) + data[x][y] * ((beachColour[2] * (1 - waterDiff[x][y])) + (waterColour[2] * (waterDiff[x][y]))));

                    //col =new Color((float)((1-waterLevel) +data[x][y])*waterColour[0],(float)((1-waterLevel)+data[x][y])*waterColour[1],(float)((1-waterLevel) +data[x][y])*waterColour[2]);
                } else if (beach[x][y]) {
                    if (forest[x][y]) {
                        col = new Color(
                                (float) data[x][y] * ((forestColour[0] * (1 - beachDiff[x][y])) + (beachColour[0] * (beachDiff[x][y]))),
                                (float) data[x][y] * ((forestColour[1] * (1 - beachDiff[x][y])) + (beachColour[1] * (beachDiff[x][y]))),
                                (float) data[x][y] * ((forestColour[2] * (1 - beachDiff[x][y])) + (beachColour[2] * (beachDiff[x][y]))));
                    } else {
                        col = new Color(
                                (float) data[x][y] * ((groundColour[0] * (1 - beachDiff[x][y])) + (beachColour[0] * (beachDiff[x][y]))),
                                (float) data[x][y] * ((groundColour[1] * (1 - beachDiff[x][y])) + (beachColour[1] * (beachDiff[x][y]))),
                                (float) data[x][y] * ((groundColour[2] * (1 - beachDiff[x][y])) + (beachColour[2] * (beachDiff[x][y]))));
                    }
                    //col =new Color((float)(data[x][y])*beachColour[0],(float)(data[x][y])*beachColour[1],(float)(data[x][y])*beachColour[2]);
                } else if (mountain[x][y]) {
                    if (forest[x][y]) {
                        col = new Color(
                                (float) (data[x][y] - 0.2f * mountainDiff[x][y]) * ((mountainColour[0] * (mountainDiff[x][y])) + (forestColour[0] * (1 - mountainDiff[x][y]))),
                                (float) (data[x][y] - 0.2f * mountainDiff[x][y]) * ((mountainColour[1] * (mountainDiff[x][y])) + (forestColour[1] * (1 - mountainDiff[x][y]))),
                                (float) (data[x][y] - 0.2f * mountainDiff[x][y]) * ((mountainColour[2] * (mountainDiff[x][y])) + (forestColour[2] * (1 - mountainDiff[x][y]))));
                    } else {
                        col = new Color(
                                (float) (data[x][y] - 0.2f * mountainDiff[x][y]) * ((mountainColour[0] * (mountainDiff[x][y])) + (groundColour[0] * (1 - mountainDiff[x][y]))),
                                (float) (data[x][y] - 0.2f * mountainDiff[x][y]) * ((mountainColour[1] * (mountainDiff[x][y])) + (groundColour[1] * (1 - mountainDiff[x][y]))),
                                (float) (data[x][y] - 0.2f * mountainDiff[x][y]) * ((mountainColour[2] * (mountainDiff[x][y])) + (groundColour[2] * (1 - mountainDiff[x][y]))));
                    }
                } else {

                    col = new Color(
                            (float) data[x][y] * groundColour[0],
                            (float) data[x][y] * groundColour[1],
                            (float) data[x][y] * groundColour[2]);

                }
                //col =new Color((float)data[x][y],(float)data[x][y],(float)data[x][y]);
                image.setRGB(x, y, col.getRGB());
            }
        }
        System.out.println(max);
        System.out.println(min);

        /*try {
            // retrieve image
           // File outputfile = new File("saved.png");
            //outputfile.createNewFile();

            //ImageIO.write(image, "png", outputfile);
        } catch (IOException e) {
            //o no!
        }*/
        return image;
    }


    private static boolean[][] newForest(int size) {
        HeightMap grass = new HeightMap(size, 0);
        grass.AddPerlinNoise(15.0f);
        grass.Perturb(30.0f, 30.0f);

        float[][] grassmap = grass.Heights;
        boolean[][] res = new boolean[size][size];
        float max = Float.MIN_VALUE;
        float min = Float.MAX_VALUE;
        for (int y = 0; y < grassmap[0].length; y++) {
            for (int x = 0; x < grassmap.length; x++) {
                if (grassmap[x][y] < forestLevel) {
                    res[x][y] = true;
                }
            }
        }


        return res;
    }


    private static float[][] newGrass(int size) {
        HeightMap grass = new HeightMap(size, 0);
        grass.AddPerlinNoise(900.0f);
        grass.Perturb(100.0f, 100.0f);

        float[][] grassmap = grass.Heights;

        float max = Float.MIN_VALUE;
        float min = Float.MAX_VALUE;
        for (int y = 0; y < grassmap[0].length; y++) {
            for (int x = 0; x < grassmap.length; x++) {

                if (grassmap[x][y] > max) {
                    max = grassmap[x][y];
                }
                if (grassmap[x][y] < min) {
                    min = grassmap[x][y];
                }
            }
        }


        for (int y = 0; y < grassmap[0].length; y++) {
            for (int x = 0; x < grassmap.length; x++) {

                grassmap[x][y] = (grassmap[x][y] - min) / (max - min);
                if (grassmap[x][y] > 1.0) {

                    grassmap[x][y] = 1;
                }
                if (grassmap[x][y] < 0) {

                    grassmap[x][y] = 0;
                }
            }
        }


        return grassmap;
    }


    public static float[][] newMap(int size) {
        HeightMap grass = new HeightMap(size, System.currentTimeMillis());
        grass.AddPerlinNoise(10f);
        grass.Perturb(10.0f, 10.0f);

        return grass.Heights;
    }
}