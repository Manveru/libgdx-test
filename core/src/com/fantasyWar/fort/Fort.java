package com.fantasyWar.fort;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btCapsuleShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.fantasyWar.base.FwActor;
import com.fantasyWar.enums.Objects;
import com.fantasyWar.unit.UnitInfo;

/**
 * Created by Michel on 15-5-2014.
 */
public class Fort extends FwActor {
    public Fort(int id, Model model, Vector3 position) {
        super(id, model, position);

        shape = new btCapsuleShape(1f, 1f);

        constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(0.0f, groundMotionState, shape, new Vector3());
        constructionInfo.obtain();


        SetPhysics(position, 0.0f);

        UnitInfo info = new UnitInfo();
        info.damage = 10;
        info.id = id;
        info.type = Objects.FORT;
        constructionInfo.setRollingFriction(10f);

        constructionInfo.setFriction(10f);
        constructionInfo.setAngularDamping(10f);
        constructionInfo.setLinearDamping(10f);
        physicsModel.userData = info;
        physicsModel.setUserValue(id);
    }
}
