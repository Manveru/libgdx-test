package com.fantasyWar.projectile;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.fantasyWar.base.FwActor;
import com.fantasyWar.enums.DamageType;
import com.fantasyWar.enums.Objects;
import com.fantasyWar.unit.UnitInfo;

/**
 * Created by Michel on 30-4-2014.
 */
public class Ball extends FwActor {

    private final DamageType damageType;

    public Ball(int id, Model model, Vector3 position) {
        super(id, model, position);
        shape = new btSphereShape(0.1f);

        constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(0.1f, groundMotionState, shape, new Vector3());
        constructionInfo.obtain();


        SetPhysics(position, 0.1f);

        damageType = DamageType.BLUDGEONING;

        UnitInfo info = new UnitInfo();
        info.damage = 10;
        info.id = id;
        info.type = Objects.BALL;
        constructionInfo.setRollingFriction(100000f);

        constructionInfo.setFriction(100000f);
        constructionInfo.setAngularDamping(100f);
        constructionInfo.setLinearDamping(100f);
        physicsModel.userData = info;
        physicsModel.setUserValue(id);

    }


}
