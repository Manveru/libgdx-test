package com.fantasyWar.unit;

import com.fantasyWar.enums.Objects;

/**
 * Created by Michel on 2-5-2014.
 */
public class UnitInfo {
    public int id;
    public Objects type;
    public int squadId;
    public int damage;
}
