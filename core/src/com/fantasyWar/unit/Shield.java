package com.fantasyWar.unit;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.physics.bullet.linearmath.btDefaultMotionState;
import com.badlogic.gdx.physics.bullet.linearmath.btMotionState;
import com.fantasyWar.enums.Objects;
import com.fantasyWar.enums.ShieldEnum;

/**
 * Created by Michel on 7-5-2014.
 */
public class Shield extends ModelInstance {


    public final Vector3 center = new Vector3();
    public final Vector3 dimensions = new Vector3();
    public final float radius;
    public final BoundingBox bounds = new BoundingBox();
    public Vector3 target;
    public boolean move;
    public btRigidBody physicsModel;
    private int health = 10;
    private btCollisionShape shape;
    private btMotionState groundMotionState;
    private btRigidBody.btRigidBodyConstructionInfo constructionInfo;
    private int id;
    private int squadId;

    public Shield(int id, Model model, Vector3 position, int squadId, ShieldEnum shieldEnum) {
        super(model, position);
        this.id = id;
        this.squadId = squadId;
        calculateBoundingBox(bounds);
        center.set(bounds.getCenter());
        dimensions.set(bounds.getDimensions());
        radius = dimensions.len() / 1.5f;
        shape = new btBoxShape(dimensions);
        groundMotionState = new btDefaultMotionState(
                new Matrix4(
                        position, //Position
                        new Quaternion(0, 0, 0, 1), //Rotation
                        new Vector3(1, 1, 1))
        ); //Scale

        Vector3 inertia = new Vector3();
        shape.calculateLocalInertia(1f, inertia);

        constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(1, groundMotionState, shape, new Vector3(0, 0, 0));
        constructionInfo.obtain();
        constructionInfo.setAngularDamping(0.5f);
        constructionInfo.setRestitution(0.5f);
        physicsModel = new btRigidBody(constructionInfo);
        physicsModel.obtain();

        UnitInfo info = new UnitInfo();
        info.damage = health;
        info.id = id;
        info.squadId = squadId;
        info.type = Objects.UNIT;
        physicsModel.userData = info;
        physicsModel.setUserValue(id);


    }
}
