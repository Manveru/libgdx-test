package com.fantasyWar.unit;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.physics.bullet.linearmath.btDefaultMotionState;
import com.badlogic.gdx.physics.bullet.linearmath.btMotionState;
import com.fantasyWar.enums.Objects;
import com.fantasyWar.enums.ShieldEnum;
import com.fantasyWar.enums.UnitState;
import com.fantasyWar.world.World;

/**
 * Created by Michel on 27-4-2014.
 */
public class Unit extends ModelInstance {

    public final Vector3 center = new Vector3();
    public final Vector3 dimensions = new Vector3();
    public final float radius;
    public final BoundingBox bounds = new BoundingBox();
    public Vector3 target;
    public btRigidBody physicsModel;
    public UnitState state = UnitState.IDLE;
    private int health = 10;
    private btCollisionShape shape;
    private btMotionState groundMotionState;
    private btRigidBody.btRigidBodyConstructionInfo constructionInfo;
    private int id;
    private int squadId;
    private Shield shield;
    private World world;

    public Unit(int id, Model model, Vector3 position, World world, int squadId, ShieldEnum shieldEnum) {
        super(model, position);
        this.id = id;
        this.squadId = squadId;
        this.world = world;
        calculateBoundingBox(bounds);
        center.set(bounds.getCenter());
        dimensions.set(bounds.getDimensions());
        //System.out.println(dimensions + " " + position);
        radius = dimensions.len() / 1.5f;
        shape = new btBoxShape(dimensions);
        groundMotionState = new btDefaultMotionState(
                new Matrix4(
                        position, //Position
                        new Quaternion(0, 0, 0, 1), //Rotation
                        new Vector3(1, 1, 1))
        ); //Scale

        Vector3 inertia = new Vector3();
        shape.calculateLocalInertia(1f, inertia);

        constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(1, groundMotionState, shape, inertia);
        constructionInfo.obtain();
        constructionInfo.setAngularDamping(1.0f);
        constructionInfo.setRestitution(0.0f);
        constructionInfo.setRollingFriction(1.0f);
        constructionInfo.setAdditionalAngularDampingFactor(0.5f);
        physicsModel = new btRigidBody(constructionInfo);
        physicsModel.obtain();

        UnitInfo info = new UnitInfo();
        info.damage = health;
        info.id = id;
        info.squadId = squadId;
        info.type = Objects.UNIT;
        physicsModel.userData = info;
        physicsModel.setUserValue(id);
    }

    public void setTarget(Vector3 in) {
        target = in;
        state = UnitState.ROTATE;
    }

    public void act(float delta) {
        Vector3 position = new Vector3();
        Vector3 newPos = physicsModel.getCenterOfMassPosition();
        transform.getTranslation(position);
        float height = world.getHeight(position.x, position.z);
        //System.out.println(newPos.y + " " + position.y + " " + height);
        if (target != null && state == UnitState.ROTATE) {
/*            Vector3 position = new Vector3();

            transform.getTranslation(position);
            Quaternion q1 = new Quaternion();
            transform.getRotation(q1);
            System.out.println("Rotation " + q1);
            //q.setFromCross(position, target);
            //q.set(0, 1, 0, 1);
            Vector3 vec = new Vector3(position);
            Quaternion q2 = new Quaternion();

            transform.getRotation(q2);
            System.out.println("Rotation2: " + q2);
            //Vector3 lookat = new Vector3();
*/
            state = UnitState.MOVE;
        }

        if (target != null && state == UnitState.MOVE) {


            Vector3 movement = new Vector3();
            movement.set(target);
            movement.sub(position);

            float length = position.dst(target);
            float scale = 1 / length;
            movement.scl((delta * scale));

            Vector3 fiMovement = new Vector3();
            fiMovement.set(position);
            fiMovement.sub(movement);
            Vector3 fin = new Vector3(position);
            fin.sub(fiMovement);


            physicsModel.translate(new Vector3(fin.x, 0.0f, fin.z));
            // transform.setTranslation(position.x, height, position.z);
            //fin.y = 0;
            // physicsModel.translate(fin);
            //transform.setToLookAt(target, new Vector3(0,0,0));
            // System.out.println(position.dst(target));
            //System.out.println("pos " + position.y);
            //physicsModel.setCenterOfMassTransform(new Matrix4(fin, new Quaternion(), new Vector3(1,1,1)));
            if (position.dst(target) < 0.1f) {
                target = null;
                state = UnitState.IDLE;
            }


        } else {
            target = null;
            state = UnitState.IDLE;
        }

        newPos = physicsModel.getCenterOfMassPosition();
        height = world.getHeight(newPos.x, newPos.z);
        //System.out.println("phys" + newPos.y);
        transform.setTranslation(newPos.x, height, newPos.z);
    }

    public int getId() {
        return id;
    }

    public void damage(int damage) {
        health = health - damage;
        if (health < 0) {
            health = 0;
        }
    }

    public int getHealth() {
        return health;
    }

    public void dispose() {
        System.out.println("removed");
        physicsModel.dispose();
    }


    public int getSquadId() {
        return squadId;
    }
}
