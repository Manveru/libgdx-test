package com.fantasyWar.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.physics.bullet.dynamics.*;
import com.badlogic.gdx.physics.bullet.linearmath.btDefaultMotionState;
import com.badlogic.gdx.physics.bullet.linearmath.btMotionState;
import com.badlogic.gdx.utils.Array;
import com.fantasyWar.enums.Objects;
import com.fantasyWar.fort.Fort;
import com.fantasyWar.projectile.Ball;
import com.fantasyWar.squads.Squad;
import com.fantasyWar.unit.Unit;
import com.fantasyWar.unit.UnitInfo;
import com.fantasyWar.utils.MapCreator;
import com.fantasyWar.utils.Randomizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Michel on 27-4-2014.
 */
public class World extends ContactListener {
    private static final float[] lightPosition = {5, 35, 5};
    private static final float[] ambientColor = {0.2f, 0.2f, 0.2f, 1.0f};
    private static final float[] diffuseColor = {0.5f, 0.5f, 0.5f, 1.0f};
    private static final float[] specularColor = {0.7f, 0.7f, 0.7f, 1.0f};
    private static final float[] fogColor = {0.2f, 0.1f, 0.6f, 1.0f};
    private static Array<Ball> arrows = new Array<Ball>();
    private final Matrix3 normalMatrix = new Matrix3();
    private final String vertexShader =
            "attribute vec4 a_position; \n" +
                    "attribute vec3 a_normal; \n" +
                    "attribute vec2 a_texCoord; \n" +
                    "attribute vec4 a_color; \n" +

                    "uniform mat4 u_MVPMatrix; \n" +
                    "uniform mat3 u_normalMatrix; \n" +

                    "uniform vec3 u_lightPosition; \n" +

                    "varying float intensity; \n" +
                    "varying vec2 texCoords; \n" +
                    "varying vec4 v_color; \n" +

                    "void main() { \n" +
                    "    vec3 normal = normalize(u_normalMatrix * a_normal); \n" +
                    "    vec3 light = normalize(u_lightPosition); \n" +
                    "    intensity = max( dot(normal, light) , 0.0); \n" +

                    "    v_color = a_color; \n" +
                    "    texCoords = a_texCoord; \n" +

                    "    gl_Position = u_MVPMatrix * a_position; \n" +
                    "}";
    private final String fragmentShader =
            "#ifdef GL_ES \n" +
                    "precision mediump float; \n" +
                    "#endif \n" +

                    "uniform vec4 u_ambientColor; \n" +
                    "uniform vec4 u_diffuseColor; \n" +
                    "uniform vec4 u_specularColor; \n" +

                    "uniform sampler2D u_texture; \n" +
                    "varying vec2 texCoords; \n" +
                    "varying vec4 v_color; \n" +

                    "varying float intensity; \n" +

                    "void main() { \n" +
                    "    gl_FragColor = v_color * intensity * texture2D(u_texture, texCoords); \n" +
                    "}";
    public Array<Squad> squads;
    public Array<Fort> forts = new Array<Fort>();
    public btDynamicsWorld dynamicsWorld;
    private Vector3 position = new Vector3();
    private btBroadphaseInterface broadphase;
    private btCollisionConfiguration btCollisionConfiguration;
    private btCollisionDispatcher btCollisionDispatcher;
    private btConstraintSolver btConstraintSolver;
    private btCollisionShape groundShape;
    private btMotionState groundMotionState;
    private btRigidBody.btRigidBodyConstructionInfo groundbodyConstructionInfo;
    private btRigidBody groundRidgidBody;
    private int id = 0;
    private TerrainChunk chunk;
    private Mesh mesh;
    private Queue<Unit> toDelete = new LinkedList<Unit>();
    private ShaderProgram shader;
    private Texture terrainTexture;
    private Matrix4 model = new Matrix4();
    private Matrix4 modelView = new Matrix4();
    private Model worldModel = new Model();
    private List<float[]> meshes;

    public World() {
        terrainTexture = new Texture(Gdx.files.internal("space.jpg"));

        squads = new Array<Squad>();
        int vertexSize = 3 + 3 + 1 + 2;
        chunk = new TerrainChunk(32, 32, vertexSize, MapCreator.newMap(33));

        mesh = new Mesh(true, chunk.vertices.length / 3, chunk.indices.length,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
                new VertexAttribute(VertexAttributes.Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE),
                new VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE));

        mesh.setVertices(chunk.vertices);
        mesh.setIndices(chunk.indices);
        mesh.transform(new Matrix4(new Vector3(-10, 0, -10), new Quaternion(), new Vector3(1, 1, 1)));


        //worldModel = ModelBuilder.createFromMesh(mesh, GL20.GL_TRIANGLES, new Material(TextureAttribute.createDiffuse(terrainTexture),

        //ColorAttribute.createSpecular(Color.WHITE), FloatAttribute.createShininess(64f), IntAttribute.createCullFace(0)));

        worldModel = new Model();
        worldModel.meshes.add(mesh);

        ShaderProgram.pedantic = false;

        shader = new ShaderProgram(vertexShader, fragmentShader);

        setPhysics();
    }

    private void setPhysics() {
        broadphase = new btDbvtBroadphase();
        broadphase.obtain();
        btCollisionConfiguration = new btDefaultCollisionConfiguration();
        btCollisionConfiguration.obtain();
        btCollisionDispatcher = new btCollisionDispatcher(btCollisionConfiguration);
        btCollisionDispatcher.obtain();
        btConstraintSolver = new btSequentialImpulseConstraintSolver();
        btConstraintSolver.obtain();
        dynamicsWorld = new btDiscreteDynamicsWorld(btCollisionDispatcher, broadphase, btConstraintSolver, btCollisionConfiguration);
        dynamicsWorld.setGravity(new Vector3(0, -12, 0));
        dynamicsWorld.obtain();


        MeshPart meshPart = new MeshPart();
        meshPart.id = "1";
        meshPart.indexOffset = 0;
        meshPart.numVertices = mesh.getNumVertices();
        meshPart.primitiveType = GL20.GL_TRIANGLES;
        meshPart.mesh = mesh;

        Array<MeshPart> meshParts = new Array<MeshPart>();
        meshParts.add(meshPart);
        groundShape = new btBvhTriangleMeshShape(meshParts);

        groundShape.obtain();

        groundMotionState = new btDefaultMotionState(
                new Matrix4(
                        new Vector3(0, -0.25f, 0), //Position
                        new Quaternion(0, 0, 0, 1), //Rotation
                        new Vector3(1, 1, 1))
        ); //Scale
        groundMotionState.obtain();
        groundbodyConstructionInfo = new btRigidBody.btRigidBodyConstructionInfo(0, groundMotionState, groundShape, new Vector3(0, 0, 0));
        groundbodyConstructionInfo.obtain();
        groundbodyConstructionInfo.setRollingFriction(10);
        groundbodyConstructionInfo.setAdditionalAngularDampingFactor(5.5f);
        groundbodyConstructionInfo.setRestitution(100.00f);
        groundbodyConstructionInfo.setAngularDamping(500f);
        groundbodyConstructionInfo.setLinearDamping(500f);
        groundbodyConstructionInfo.setFriction(10);
        groundRidgidBody = new btRigidBody(groundbodyConstructionInfo);
        groundRidgidBody.obtain();
        //groundRidgidBody.setUserValue(Objects.WORLD.getValue());
        groundRidgidBody.setUserValue(getId());
        //groundRidgidBody.setRollingFriction(0.5f);
        dynamicsWorld.addRigidBody(groundRidgidBody);


    }

    public void addSquad(Squad newSquad) {
        for (Unit unit : newSquad.units) {
            dynamicsWorld.addRigidBody(unit.physicsModel);
        }
        squads.add(newSquad);
    }

    public void render(float delta, Camera cam, ModelBatch modelBatch, Environment environment) {
        model.setToRotation(new Vector3(0, 1, 0), 45f);
        modelView.set(cam.view).mul(model);

        terrainTexture.bind();
        shader.begin();

        shader.setUniformMatrix("u_MVPMatrix", cam.combined);
        shader.setUniformMatrix("u_normalMatrix", normalMatrix.set(modelView).inv().transpose());

        shader.setUniform3fv("u_lightPosition", lightPosition, 0, 3);
        shader.setUniform4fv("u_ambientColor", ambientColor, 0, 4);
        shader.setUniform4fv("u_diffuseColor", diffuseColor, 0, 4);
        shader.setUniform4fv("u_specularColor", specularColor, 0, 4);

        shader.setUniformi("u_texture", 0);

        mesh.render(shader, GL20.GL_TRIANGLES);


        for (Squad instance : squads) {
            instance.render(delta, cam, modelBatch, environment);
        }


        for (Fort instance : forts) {
            modelBatch.render(instance, environment);
        }

        for (Ball instance : arrows) {
            modelBatch.render(instance, environment);
        }
    }

    public void act(float delta) {
        dynamicsWorld.stepSimulation(1.0f / 60.0f);
        for (int i = 0; i < squads.size; i++) {
            squads.get(i).act(delta);
        }
        for (Ball instance : arrows) {
            instance.act(delta);
        }

    }

    public void clear() {

    }


    public void fire(Squad squad, Squad targetSquad) {


        ModelBuilder builder = new ModelBuilder();
        Vector3 forceDir = new Vector3(targetSquad.position);
        forceDir.sub(squad.position);
        float distance = squad.position.dst(targetSquad.position);

        for (Unit unit : squad.units) {
            Vector3 pos = new Vector3();
            unit.transform.getTranslation(pos);

            float randomX = Randomizer.RandomValue(-1f, 1f);
            float randomY = Randomizer.RandomValue(-1f, 1f);
            float randomZ = Randomizer.RandomValue(-1f, 1f);
            Model ballModel = builder.createSphere(0.1f, 0.1f, 0.1f, 10, 10, new Material(ColorAttribute.createDiffuse(new Color(0, 1.0f, 0, 1))),
                    VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
            pos.y = pos.y + 1;
            // System.out.println("Fire Y " + pos.y);
            Ball ball = new Ball(getId(), ballModel, pos);

            float y = Math.min(distance, 12);

            arrows.add(ball);
            dynamicsWorld.addRigidBody(ball.physicsModel);
            ball.physicsModel.applyForce(new Vector3(forceDir.x * (8 + randomX), y * (6 + randomY), forceDir.z * (8 + randomZ)), new Vector3());
        }

    }

    @Override
    public void onContactStarted(btCollisionObject colObj0, btCollisionObject colObj1) {

        if (colObj0.getUserValue() != 0 && colObj1.getUserValue() != 0) {
            UnitInfo obj1 = (UnitInfo) colObj0.userData;
            UnitInfo obj2 = (UnitInfo) colObj1.userData;
            if (obj1 != null && obj2 != null) {
                Unit unit = null;
                Squad squad = null;
                if (obj1.type == Objects.BALL && obj2.type == Objects.UNIT) {

                    if (colObj0.getInterpolationLinearVelocity().len2() > 10) {
                        squad = squads.get(getSquad(obj2.squadId));

                        unit = squad.attack(obj2.id, obj1);
                    }

                } else if (obj1.type == Objects.UNIT && obj2.type == Objects.BALL) {
                    if (colObj1.getInterpolationLinearVelocity().len2() > 10) {
                        squad = squads.get(getSquad(obj1.squadId));
                        unit = squad.attack(obj1.id, obj2);
                    }
                }
                if (unit != null) {
                    toDelete.add(unit);
                }
            }

        }

    }

    public int getSquad(int id) {
        for (int i = 0; i < squads.size; i++) {
            if (squads.get(i).getId() == id) {
                return i;
            }

        }
        return -1;
    }

    @Override
    public void onContactProcessed(int userValue0, int userValue1) {
        while (!toDelete.isEmpty()) {
            Unit unit = toDelete.poll();
            int index = getSquad(unit.getSquadId());
            Squad squad = squads.get(index);

            if (squad != null) {
                dynamicsWorld.removeRigidBody(unit.physicsModel);
                unit.dispose();
                squad.remove(unit);


                if (squad.units.size == 0) {
                    //System.out.println(squad.units.size + " " + index);
                    squad.dispose();
                    squads.removeIndex(index);
                }
            }
        }
    }

    public int getId() {
        int returnId = id;
        id++;
        return returnId;
    }

    public float[] getVertices() {
        float[] meshes = new float[mesh.getNumVertices() * mesh.getVertexSize()];
        mesh.getVertices(meshes);


        return meshes;
    }

    public short[] getIndices() {
        short[] meshes = new short[mesh.getNumIndices()];
        mesh.getIndices(meshes);
        return meshes;
    }

    public int getVertexSize() {

        return mesh.getVertexSize();

    }

    public float getHeight(float x, float z) {
        Ray ray = new Ray(new Vector3(x, 100, z), new Vector3(0, -1, 0));

        Vector3 intersection = new Vector3(0, -1, 0);

        Intersector.intersectRayTriangles(ray, getVertices(), getIndices(), getVertexSize(), intersection);
        //System.out.println(intersection);
        return intersection.y;
    }

    public void addFort(Fort fort) {
        forts.add(fort);
    }
}
