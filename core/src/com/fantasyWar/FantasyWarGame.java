package com.fantasyWar;

import com.badlogic.gdx.*;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.fantasyWar.enums.ShieldEnum;
import com.fantasyWar.enums.SquadState;
import com.fantasyWar.fort.Fort;
import com.fantasyWar.squads.MeleeSquad;
import com.fantasyWar.squads.RangedSquad;
import com.fantasyWar.squads.Squad;
import com.fantasyWar.unit.Unit;
import com.fantasyWar.world.World;


public class FantasyWarGame extends InputAdapter implements ApplicationListener, InputProcessor {

    protected PerspectiveCamera cam;
    protected CameraInputController camController;
    protected ModelBatch modelBatch;
    protected AssetManager assets;
    //protected Array<Unit> instances = new Array<Unit>();
    protected Environment environment;
    protected boolean loading;
    protected World world;
    protected Stage stage;
    protected Label label;
    protected BitmapFont font;
    protected StringBuilder stringBuilder;
    protected  ModelBuilder builder = new ModelBuilder();
    private Squad selectedSquad;
    private int visibleCount;
    private ShapeRenderer shapeRenderer;
    private boolean targeting = false;
    private int mouseX;
    private int mouseY;
    private int selected = -1, selecting = -1;
    private TextButton placeUnit;
    private TextButton.TextButtonStyle textButtonStyle;
    private SpriteBatch batch;
    @Override
    public void create ()
    {
        Bullet.init(true);

        stringBuilder = new StringBuilder();
        shapeRenderer = new ShapeRenderer();
        modelBatch = new ModelBatch();
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        // GUI
        stage = new Stage();
        font = new BitmapFont();
        label = new Label(" ", new Label.LabelStyle(font, Color.WHITE));
        stage.addActor(label);
        Gdx.input.setInputProcessor(stage);
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        textButtonStyle.fontColor = Color.WHITE;
        placeUnit = new TextButton("Place unit", textButtonStyle);
        placeUnit.setPosition(10, 50);
        placeUnit.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("Button Pressed");
            }
        });
        stage.addActor(placeUnit);

        // Cam
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(0f, 7f, 10f);
        cam.lookAt(0, 0, 0);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();

        camController = new CameraInputController(cam);
        Gdx.input.setInputProcessor(new InputMultiplexer(this, camController));

        assets = new AssetManager();
        assets.load("unit.g3db", Model.class);
        assets.load("fort.g3db", Model.class);
        assets.load("selection.png", Texture.class);
        loading = true;
        batch = new SpriteBatch();
        world = new World();
    }

    private void doneLoading () {

        createRangedSquad(0, new Vector3(-6, 1, 0));
        createMeleeSquad(1, new Vector3(6, 1, 0));
        createFort();
        loading = false;
    }

    public void createFort() {
        int squadId = world.getId();

        Model model = assets.get("fort.g3db", Model.class);

        float height = world.getHeight(0, 0);
        Fort fort = new Fort(squadId, model, new Vector3(0, height, 0));
        world.addFort(fort);
    }

    public void createRangedSquad(int team, Vector3 position)
    {
        int squadId = world.getId();
        Array<Unit> instances = new Array<Unit>();
        Model model = assets.get("unit.g3db", Model.class);
        Vector3 pos = new Vector3();
        pos.y = position.y;
        pos.x = position.x + -0.5f;
        pos.z = position.z + 0f;
        Unit instance = new Unit(world.getId(), model, pos, world, squadId, ShieldEnum.NONE);
        instances.add(instance);

        Vector3 pos2 = new Vector3();
        pos2.y = position.y;
        pos2.x = position.x + 0.5f;
        pos.z = position.z + 0f;
        Unit instance2 = new Unit(world.getId(), model, pos2, world, squadId, ShieldEnum.NONE);
        instances.add(instance2);

        Vector3 pos3 = new Vector3();
        pos3.y = position.y;
        pos3.x = position.x + 0.0f;
        pos3.z = position.z +  0.75f;

        Unit instance3 = new Unit(world.getId(), model, pos3, world, squadId, ShieldEnum.NONE);
        instances.add(instance3);

        Vector3 pos4 = new Vector3();
        pos4.y = position.y;
        pos4.x = position.x + 0f;
        pos4.z = position.z + 0.25f;

        Unit instance4 = new Unit(world.getId(), model, pos4, world, squadId, ShieldEnum.NONE);
        instances.add(instance4);

        RangedSquad squad = new RangedSquad(squadId, instances, "Archers", stage, team, world);

        world.addSquad(squad);
    }

    public void createMeleeSquad(int team, Vector3 position) {
        int squadId = world.getId();
        Array<Unit> instances = new Array<Unit>();
        Model model = assets.get("unit.g3db", Model.class);
        Vector3 pos = new Vector3();
        pos.y = position.y + 0.5f;
        pos.x = position.x + -0.5f;
        pos.z = position.z + 0f;
        Unit instance = new Unit(world.getId(), model, pos, world, squadId, ShieldEnum.TOWER);
        instances.add(instance);

        Vector3 pos2 = new Vector3();
        pos2.y = position.y + 0.5f;
        pos2.x = position.x + 0.5f;
        pos.z = position.z + 0f;
        Unit instance2 = new Unit(world.getId(), model, pos2, world, squadId, ShieldEnum.TOWER);
        instances.add(instance2);

        Vector3 pos3 = new Vector3();
        pos3.y = position.y + 0.5f;
        pos3.x = position.x + 0.0f;
        pos3.z = position.z + 0.75f;

        Unit instance3 = new Unit(world.getId(), model, pos3, world, squadId, ShieldEnum.TOWER);
        instances.add(instance3);

        Vector3 pos4 = new Vector3();
        pos4.y = position.y + 0.5f;
        pos4.x = position.x + 0f;
        pos4.z = position.z + 0.25f;

        Unit instance4 = new Unit(world.getId(), model, pos4, world, squadId, ShieldEnum.TOWER);
        instances.add(instance4);

        MeleeSquad squad = new MeleeSquad(squadId, instances, "Swordmen", stage, team, world);

        world.addSquad(squad);
    }

    @Override
    public void render () {
        if (loading && assets.update()) doneLoading();

        float delta = Gdx.graphics.getDeltaTime();


        //System.out.println(delta);
        //physics do run fixed 60times per second
        physics(delta);

        // logic do run fixed 15times per second
        logic(delta);

        //this is done allways, everything rendered is also interpolated between two physic state
        render(delta);
    }

    private void physics(float delta) {

    }

    private void logic(float delta)
    {
        world.act(delta);
    }

    private void render(float delta)
    {
        camController.update();

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glEnable(GL20.GL_BLEND);


        modelBatch.begin(cam);

        world.render(delta, cam, modelBatch, environment);

        visibleCount = 0;

        modelBatch.end();

        stringBuilder.setLength(0);
        stringBuilder.append(" FPS: ").append(Gdx.graphics.getFramesPerSecond());
        stringBuilder.append(" Selected: ").append(selected);
        label.setText(stringBuilder);
        stage.draw();


        if (selectedSquad != null) {
            Vector3 squadPosition = new Vector3(selectedSquad.position);
            float dist = cam.position.dst(squadPosition);
            if (dist > 2) {

                Texture texture = assets.get("selection.png", Texture.class);

                Vector3 proj = cam.project(squadPosition);

                //System.out.println(cam.position + " " + selectedSquad.position);
                float range = Math.max(5, (30 - dist) + 10 * (7f / (dist)));

                batch.begin();
                batch.draw(texture, proj.x - range, proj.y + range);
                batch.draw(texture, proj.x + range, proj.y + range, 16, 16, 0, 0, 16, 16, true, false);
                batch.draw(texture, proj.x - range, proj.y - range, 16, 16, 0, 0, 16, 16, false, true);
                batch.draw(texture, proj.x + range, proj.y - range, 16, 16, 0, 0, 16, 16, true, true);
                batch.end();
            }
        }
        if (selectedSquad != null && selectedSquad.state == SquadState.MOVING) {

            Vector3 target = selectedSquad.getTarget();
            Vector3 pos = selectedSquad.position;
            //System.out.println("Line");
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.GREEN);
            shapeRenderer.line(target.x, 0.5f, target.z, pos.x, 0.5f, pos.z);
            shapeRenderer.end();
        }
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {

        mouseX = screenX;
        mouseY = screenY;
        return super.mouseMoved(screenX, screenY);

    }

    @Override
    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        if (button == 0) {
            selecting = getObject(screenX, screenY);
            return selecting >= 0;
        } else {
            selecting = -1;
            return false;
        }

    }

    @Override
    public boolean touchDragged (int screenX, int screenY, int pointer) {
        return selecting >= 0;
    }

    @Override
    public boolean touchUp (int screenX, int screenY, int pointer, int button)
    {
        if (button == 0) {
            if (selecting >= 0) {
                setSelected(selecting);
                selecting = -1;
                return true;
            } else {
                Vector3 move = getPosition(screenX, screenY);
                if (move.y != -1.0 && selected > -1) {
                    world.squads.get(selected).setTarget(move);
                }
            }
            return false;
        }
        else if(button == 1 && selected >=0)
        {
            selectedSquad = null;
            selected = -1;

        }
        return false;
    }

    public void setSelected (int value) {
        if (selected == value) return;
        if (value >= 0) {
            selected = value;
            selectedSquad = world.squads.get(selected);

        }
    }

    public int getObject (int screenX, int screenY) {
        Ray ray = cam.getPickRay(screenX, screenY);
        int result = -1;

        for (int i = 0; i < world.squads.size; ++i)
        {
            final Squad instance = world.squads.get(i);

            if (Intersector.intersectRaySphere(ray, instance.position, instance.radius, null)) {
                result = i;
                selectedSquad = instance;


            }
        }
        return result;
    }


    public Vector3 getPosition (int screenX, int screenY) {
        Ray ray = cam.getPickRay(screenX, screenY);
        Vector3 intersection = new Vector3(0, -1, 0);

        Intersector.intersectRayTriangles(ray, world.getVertices(), world.getIndices(), world.getVertexSize(), intersection);

        float height = world.getHeight(intersection.x, intersection.z);
        System.out.println(height);
        // System.out.println(intersection);
        return intersection;
    }

    @Override
    public void dispose () {
        modelBatch.dispose();
        world.clear();
        assets.dispose();
    }

    @Override
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause () {
    }

    @Override
    public void resume ()
    {

    }

    public Vector3 get3dPoint() {

        Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
        return cam.unproject(touchPos);

    }

    @Override
    public boolean keyUp(int keycode)
    {
        if(keycode == Input.Keys.Z)
        {
            //  world.fire(selected);
            return true;
        }
        else  if(keycode == Input.Keys.Q)
        {
            target(selected);
            return true;
        }
        else
        {
            return super.keyUp(keycode);
        }
    }


    public void target(int selected)
    {
        if (selected == -1) {
            return;
        }
        targeting = true;
        selectedSquad = world.squads.get(selected);

    }
}